# INIT SYSTEM
sudo apt upgrade -y
sudo apt update

echo iptables-persistent iptables-persistent/autosave_v4 boolean true | sudo debconf-set-selections
echo iptables-persistent iptables-persistent/autosave_v6 boolean true | sudo debconf-set-selections
sudo systemctl disable systemd-networkd-wait-online.service

# TRUST KEYS
wget -O - https://repo.jellyfin.org/debian/jellyfin_team.gpg.key | sudo apt-key add -
wget -O - http://www.webmin.com/jcameron-key.asc | sudo apt-key add -
wget -O - https://dl.ubnt.com/unifi/unifi-repo.gpg | sudo apt-key add -
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 0C49F3730359A14518585931BC711F9BA15703C6

# ADD REPOSITORIES
sudo add-apt-repository universe
sudo apt-add-repository "deb https://repo.jellyfin.org/debian ubuntu main"
sudo apt-add-repository "deb http://download.webmin.com/download/repository sarge contrib"
sudo apt-add-repository "deb http://www.ubnt.com/downloads/unifi/debian stable ubiquiti"
sudo apt-add-repository "deb http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.4 multiverse"

sudo apt update

# INSTALL PACKAGES
sudo apt install -y apt-transport-https
sudo apt install -y jellyfin webmin isc-dhcp-server unifi bind9 iptables-persistent exfat-fuse exfat-utils

# JELLYFIN CONFIG
sudo cp config/rules.v4 /etc/iptables                                                      # configure iptables redirect

# WEBMIN CONFIG
sudo cp config/dhcpd.conf /etc/dhcp                                                        # configure dhcp serverx

# NETWORKING CONFIG
sudo cp config/50-cloud-init.yaml /etc/netplan                                             # configure netplan interfaces
sudo cp config/rules.v4 /etc/iptables
sudo cp config/skyflix.com.hosts /var/lib/bind
sudo cp config/isc-dhcp-server /etc/default/ #Configure DHCP Interface
sudo cp config/named.conf.local /etc/bind
sudo cp config/bind9 /etc/default
sudo netplan apply

#Disable MOTD
sudo chmod -x /etc/update-motd.d/*

# MAKE MEDIA DIRECTORIES
sudo mkdir /SKYFLIX-MEDIA
#sudo mkdir /SKYFLIX-MEDIA/movies
#sudo mkdir /SKYFLIX-MEDIA/tv-shows
#sudo mkdir /SKYFLIX-MEDIA/music

#Remove Cloud Init
echo 'datasource_list: [ None ]' | sudo -s tee /etc/cloud/cloud.cfg.d/90_dpkg.cfg
sudo apt-get purge cloud-init -y
sudo rm -rf /etc/cloud/; sudo rm -rf /var/lib/cloud/

sudo su << EOF
    crontab -e
EOF

sudo su << EOF
    crontab -l | { cat; echo "@reboot bash /home/skyflix/start-script/sound.sh"; } | crontab - &&
    crontab -l | { cat; echo "@reboot /usr/sbin/netplan apply"; } | crontab -
EOF

sudo reboot
