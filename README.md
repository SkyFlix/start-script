# SkyFlix Start Script
Install and configure webmin, unifi, jellyfin, dhcp, and bind9 DNS.

## 1. Clone Onto Machine

``` git clone https://SkyFlix@bitbucket.org/SkyFlix/start-script.git ```

## 2. Run `install.sh`
``` bash
$ cd start-script
$ ./install.sh
```

The system will reboot at the end. It takes five minutes for the services to come back online.

## Configuration
Some common configuration items.

1. Change DNS for JellyFin
> Change `watch.skyflix.com` in config/skyflix.com.hosts _before installation_


``` $ git clone git@bitbucket.org:SkyFlix/start-script.git ```
